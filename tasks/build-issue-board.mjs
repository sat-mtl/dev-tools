#!/usr/bin/env zx
/* global $ YAML argv */
/**
 * Create an issue board for a project.
 * Arguments:
 *   ID of the project.
 *   Name of the milestone.
 * Globals:
 *   PRIVATE_TOKEN as a Personal Gitlab API access token for `read_api`.
 * This should be executed with https://github.com/google/zx
 * Article: https://flaming.codes/posts/google-zx-shell-scripts-with-javascript
 * Build from https://docs.gitlab.com/ee/api/issues.html
 **/

// this should be set to true for debugging
$.verbose = false

const GITLAB_API_ROOT = 'https://gitlab.com/api/v4'
const PRIVATE_TOKEN = process.env.PRIVATE_TOKEN

/**
 * A Gitlab issue model
 * @param {object} json - Issue model returned by the Gitlab Issue API
 */
class Issue {
  iid = null
  mrs = []
  title = null
  status = null

  static statuses = [
    'on hold',
    'in progress',
    'canceled',
    'duplicated',
    'need help',
    'need review',
    'ready to test',
    'require rework'
  ]

  constructor (json) {
    this.iid = json.iid
    this.title = json.title
    const labels = json.labels || []

    for (const label of labels) {
      if (Issue.statuses.includes(label)) {
        this.status = `~"${label}"`
        break
      }
    }

    if (!this.status) {
      this.status = '~"on hold"'
    }
  }

  toString () {
    return `| ${[
      `#${this.iid}`,
      this.mrs.map(mr => `!${mr}`).join(', '),
      this.title,
      this.status
    ].join(' | ')} |`
  }
}

/**
 * A Board model
 * @param {object} json - Issue list returned by the Gitlab Issue API
 */
class Board {
  issues = []

  static headers = [
    'Issue',
    'Merge request',
    'Name',
    'Status'
  ]

  constructor (json) {
    this.issues = json.map(obj => new Issue(obj))
  }

  toString () {
    let md = `| ${Board.headers.join(' | ')} |`
    md += `\n| ${Board.headers.map(() => '-').join(' | ')} |`

    for (const issue of this.issues) {
      md += `\n${issue.toString()}`
    }

    return md
  }
}

/**
 * Fetches all related merge requests to an issue.
 * @param {string} projectId - ID of the project
 * @param {string} issueIid - IID of the issue
 * @returns {object} An relation model as JSON
 */
async function fetchRelatedMR (projectId, issueIid) {
  const query = `${GITLAB_API_ROOT}/projects/${projectId}/issues/${issueIid}/related_merge_requests`
  const response = await $`curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" ${query}`

  let parsed = YAML.parse(response.stdout)

  if (!Array.isArray(parsed)) {
    parsed = [parsed]
  }

  return parsed
}

/**
 * Fetches all issues linked to a milestone.
 * @param {string} projectId - ID of the project
 * @param {string} milestoneName - Name of the milestone
 * @returns {object} An issue model as JSON
 */
async function fetchIssues (projectId, milestoneName) {
  const milestone = milestoneName.replace(' ', '%20')
  const query = `${GITLAB_API_ROOT}/projects/${projectId}/issues?milestone=${milestone}`
  const response = await $`curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" ${query}`

  return YAML.parse(response.stdout)
}

/**
 * Builds a board model.
 * @param {string} projectId - ID of the project
 * @param {string} milestoneName - Name of the milestone
 * @async
 * @returns {Board} A board object
 */
async function buildBoard (projectId, milestoneName) {
  const json = await fetchIssues(projectId, milestoneName) //  "21080505", "DBoxer v1.1"
  const board = new Board(json)

  for (const issue of board.issues) {
    const mergeRequests = await fetchRelatedMR(projectId, issue.iid)

    for (const json of mergeRequests) {
      issue.mrs.push(json.iid)
    }
  }

  return board
}

let isInitialized = true

if (argv._.length !== 2) {
  isInitialized = false
  console.error('PROJECT_ID and MILESTONE_NAME arguments are required')
}

if (!PRIVATE_TOKEN) {
  isInitialized = false
  console.error('also export your Personal Gitlab API access token as the PRIVATE_TOKEN environment variable')
}

if (isInitialized) {
  const board = await buildBoard(argv._[0], argv._[1])
  console.log(board.toString())
}
