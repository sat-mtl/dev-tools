#!/bin/bash

# Get the current branch name

set -u
set -o pipefail

#######################################
# Prints errors nicely
# Returns:
#   A red message
#######################################
echoError () {
  local red='\033[0;31m'
  local default='\033[0m'

  echo -e "Error: $red$1$default"
}

#######################################
# Prints infos nicely
# Returns:
#   A blue message
#######################################
echoInfo () {
  local blue='\e[0;36m'
  local default='\033[0m'

  echo -e "Info: $blue$1$default"
}

#######################################
# Check the current branch name and apply branch naming conventions :
# Each branch name needs to prefixed as:
# fix/<name>, feat/<name>, doc/<name>, intl/<name> test/<name> or candidate/<name>
#######################################
checkBranchName() {
  local prefixRegex='^(fix|feat|doc|intl|test|candidate)\/'
  local currentBranch

  currentBranch="$(git rev-parse --abbrev-ref HEAD)"

  if ! echo "${currentBranch}" | grep -qE "${prefixRegex}"; then
    echoError "Local branch name needs to be prefixed by feat, fix, doc, test, intl or candidate."
    echoInfo "Use 'git branch -m <branchname>' in order to update your branch"
    echoError "Aborting push :("
    exit 1
  fi
}

checkBranchName
